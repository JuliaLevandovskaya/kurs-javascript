# Kurs JavaScript
## Jak rozwiązywać zadania?

Wykonaj następujące kroki:

1. Stwórz [**fork**][forking] repozytorium z zadaniami.
2. [**Sklonuj**][ref-clone] repozytorium na swój komputer.
3. Rozwiąż zadania i [**skomituj**][ref-commit] zmiany do swojego repozytorium.
4. [**Wypchnij**][ref-push] zmiany do swojego repozytorium na GitHubie.
5. Stwórz [**pull request**][pull-request] do oryginalnego repozytorium, gdy skończysz wszystkie zadania.



Pamiętaj o dodawaniu komentarzy z numerem zadania, np.:

```JavaScript
// Zadanie 1
{ Kod do zadania 1 }

//Zadanie 2
{ Kod do zadania 2 }

...
```

<!-- Links -->
[forking]: https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html
[ref-clone]: https://confluence.atlassian.com/bitbucketserver/clone-a-repository-790632786.html#Clonearepository-Cloningamirrorrepository
[ref-commit]: http://gitref.org/basic/#commit
[ref-push]: http://gitref.org/remotes/#push
[pull-request]: https://confluence.atlassian.com/bitbucket/create-a-pull-request-774243413.html
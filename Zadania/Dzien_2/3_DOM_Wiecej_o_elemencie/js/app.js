document.addEventListener("DOMContentLoaded", function() {
    
//zadanie 1
var chrome = document.querySelector('.ex1 .chrome');
setLink(chrome, 'https://www.google.pl/chrome/browser/desktop/index.html', 'Chrome');
chrome.style.width = '100px';

var edge = document.querySelector('.ex1 .edge');
setLogo(edge, "../3_DOM_Wiecej_o_elemencie/assets/img/edge.png");
setLink(edge, "https://microsoft-edge.en.softonic.com", "Microsoft Edge");
    
var firefox = document.querySelector('.ex1 .firefox');
setLogo(firefox, "../3_DOM_Wiecej_o_elemencie/assets/img/firefox.png");
setLink(firefox, "https://www.mozilla.org/en-US/firefox", "Firefox");

function setLogo(element, imageUrl) {
    element.style.backgroundImage = 'url("' + imageUrl + '")';
}

function setLink(element, linkUrl, textValue) {
    var parent = element.parentElement;
    parent.querySelector('a').setAttribute('href', linkUrl);
    parent.querySelector('a').innerText = textValue;
}
//style dopisane uzywajac js sa dodawane inlinowo do elementow
    
//zadanie 2
var name = document.querySelector('.ex2 #name');
var color = document.querySelector('.ex2 #fav_color');
var meal = document.querySelector('.ex2 #fav_meal');

name.innerHTML = 'Julia';
color.innerHTML = 'black';
meal.innerHTML = 'salad';

//zadanie 3
document.querySelector('.ex3 ul').classList.add('menu');

var menuElements = document.querySelectorAll('.ex3 li');
for(var i = 0; i < menuElements.length; i++){
    menuElements[i].classList.add('menuElement');
    if(menuElements[i].classList.contains('error')) {
        menuElements[i].classList.remove('error');
    }
}

//zadanie 4
var listElements = document.querySelectorAll('.ex4 li');
for(var i = 0; i < listElements.length; i++){
    listElements[i].dataset.Id = i+1;
    listElements[i].setAttribute('data-id', i+1);
}

});

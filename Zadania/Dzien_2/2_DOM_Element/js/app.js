document.addEventListener("DOMContentLoaded", function() {
    var homeElement = document.getElementById("home");
    var childElements = document.querySelector(".oferts").children;
    var banner = document.querySelector(".ban");
    var blocks = document.querySelectorAll(".block");
    var links = document.querySelector(".links").children;

    //zadanie 1
    console.log(homeElement.innerHTML);
    console.log(childElements.innerHTML);
    console.log(banner.innerHTML);

    //lista
    console.log(blocks);
    for(var i = 0; i<blocks.length; i++){
        console.log(blocks[i].tagName);
        console.log(blocks[i].className);
    }

    console.log(links);  

    //zadanie 2
    for(var i = 0; i < blocks.length; i++){
        //innerHTML pobiera zawartosc tagu 
        console.log(blocks[i].innerHTML);
        //outerHTML pobiera zawartosc tagu razem z samym tagiem
        console.log(blocks[i].outerHTML);

        blocks[i].innerHTML = 'newValue'; //zawartosc kazdego diva zostala nadpisana nowa wartoscia
    }

    //zadanie 3
    var mainFooter = document.querySelector('#mainFooter');

    function getId(element) {
        return element.id;
    }

    console.log(getId(mainFooter));

    //zadanie 4
    function getTags(elements) {
        var childElementsArray = [];
        for(var i = 0; i < elements.length; i++){
            childElementsArray.push(elements[i].tagName);
        }
        return childElementsArray;
    }

    console.log(getTags(childElements));


    //zadanie 5
    function getClassInfo(element) {
        return element.classList;
    }

    console.log(getClassInfo(banner));

    //zadanie 6
    var navLiElements = document.querySelectorAll('nav li');

    function setDataDirection(elements) {
        for(var i = 0; i < elements.length; i++){
            if(!elements[i].hasAttribute('data-direction')){
                elements[i].setAttribute('data-direction', 'top');
            }
        }
    }
    setDataDirection(navLiElements);
});




document.addEventListener('DOMContentLoaded', function () {
    var buttons = document.querySelectorAll('a');
    for(var i = 0; i < buttons.length; i++){
        buttons[i].addEventListener('click', function () {
            var nextElement = this.nextElementSibling;
            if (nextElement !== null){
                nextElement.classList.toggle('hidden');
            } 
        });
    }
});
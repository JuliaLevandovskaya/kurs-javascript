document.addEventListener('DOMContentLoaded', function () {
    var divElements = document.querySelectorAll('div');
    for(var i = 0; i < divElements.length; i++){
        divElements[i].addEventListener('mouseover', function () {
            var list = this.firstElementChild;

            list.firstElementChild.style.backgroundColor = 'red';
            list.lastElementChild.style.backgroundColor = 'blue';

            var listElements = list.children;
            for(var i = 1; i < listElements.length-1; i++){
                listElements[i].style.backgroundColor = 'green';
            }
        });
    }
});

document.addEventListener('DOMContentLoaded', function () {

    document.querySelector('.box.big').addEventListener('mousemove', function (event) {
        document.querySelector('#globalX').innerHTML = event.clientX;
        document.querySelector('#globalY').innerHTML = event.clientY;
        document.querySelector('#localX').innerHTML = event.offsetY;
        document.querySelector('#localY').innerHTML = event.offsetY;
    });
    
});
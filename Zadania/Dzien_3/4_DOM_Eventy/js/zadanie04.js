document.addEventListener('DOMContentLoaded', function(){

    var buttons = document.querySelectorAll('button');
    for(var i = 0; i < buttons.length; i++){
        buttons[i].addEventListener('click', function () {
            var counter = document.querySelector('.counter').innerHTML;
            counter++;
            document.querySelector('.counter').innerHTML = counter;
        });
    }
});
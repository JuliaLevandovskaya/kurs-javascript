document.addEventListener("DOMContentLoaded", function () {

    document.querySelector('#button1').addEventListener('click', function () {
        var counterElement = this.nextElementSibling.querySelector('.counter');
        var counter = counterElement.innerHTML;
        counter++;
        counterElement.innerHTML = counter;
    })

    document.querySelector('#button2').addEventListener('click', function () {
        var counterElement = this.nextElementSibling.querySelector('.counter');
        var counter = counterElement.innerHTML;
        counter++;
        counterElement.innerHTML = counter;
    })

    document.querySelector('#button3').addEventListener('click', function () {
        var counterElement = this.nextElementSibling.querySelector('.counter');
        var counter = counterElement.innerHTML;
        counter++;
        counterElement.innerHTML = counter;
    })
});
var task1Array = [
    [ 2, 3 ],
    [ "Ala", "Ola" ],
    [ true, false ],
    [ 5, 6, 7, 8 ],
    [ 12, 15, 67 ]
];

var task2Array = [
      [ 1, 2, 3, 4 ],
      [ 5, 6, 7, 8 ],
      [ 9, 10, 11, 12 ]
];


//zadanie 1
//element znajdujący się w położeniu 3, 2.
// console.log(task1Array[2][1]);

// //długość tablicy znajdującej się w drugim rzędzie.
// console.log(task1Array[1].length);

// //element znajdujący się w położeniu 4, 2.
// console.log(task1Array[3][1]);

//zadanie 2

// zawartość pierwszego wymiaru naszej tablicy.
// for(var i = 0; i < task2Array.length; i++) {
//     console.log(task2Array[i]);
// }

// długość tablic składających się na 2gi wymiar
// for(var i = 0; i < task2Array.length; i++) {
//     console.log(task2Array[i].length);
// }

// wszystkie elementy tablicy 2 wymiarowej.
// for (var i = 0; i < task2Array.length; i++) {
//     for (var j = 0; j < task2Array[i].length; j++) {
//         console.log(task2Array[i][j]);
//     }
// }

//zadanie 3
// function print2DArray(array) {
//     for (var i = 0; i < array.length; i++) {
//         for (var j = 0; j < array[i].length; j++) {
//             console.log(array[i][j]); 
//         }
//     }
// }

//print2DArray(task1Array);

//zadanie 4
var array = [
    [1, 2, 3, 4],
    ['ala', 'ma', 'kota']
];

// print2DArray(array);

//zadanie 5
function create2DArray(rows, columns) {
    var array = [];
    for (var i = 0; i < rows; i++) {
        var col = [];
        for (var j = 0; j < columns; j++) {
            col.push(j*i + 1+j);
        }
        array[i] = col;
    }
    return array;
}

var array = create2DArray(2, 4);

for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
        console.log(array[i][j]);
    }
}

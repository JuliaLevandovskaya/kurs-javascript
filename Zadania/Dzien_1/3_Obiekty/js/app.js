
//zadanie 1
var book = {
    title: 'title',
    author: 'Author',
    numberOfPages: 200
};

console.log(book.title);
console.log(book.author);
console.log(book.numberOfPages);

//zadanie 2
var person = {
    name: 'John',
    age: 67,
    sayHello: function() {
        console.log('Hello');
    }
};

console.log(person.name);
console.log(person.age);

person.sayHello();

//zadanie 3
var train = {
    name: 'train'
};

console.log(train instanceof Object);

//zadanie 4
var car = {
    brand: 'Car',
    color: 'black',
    numberOfKilometers: 0,
    printCarinfo: function() {
        console.log(this.color + ' ' + this.brand + ' ' + this.numberOfKilometers);
    },
    drive: function(km) {
        this.numberOfKilometers += km; 
    }
}

car.printCarinfo();
car.drive(150);
car.printCarinfo();

//zadanie 5
car.reviews = ['2002.23.01', '2012.23.01'];

car.addReview = function (date) {
    this.reviews.push(date);
}

car.showReviews = function() {
    return this.reviews;  
}

var arr = car.showReviews();
car.addReview('2017.05.06');
arr.forEach(i => {
    console.log(i);
});

//zadanie 6
var bird = {
    type: 'bird',
    name: 'bird name',
    getType: function() {
        return this.type;
    }
}

//Obiekt bird jest instancja object. 
console.log(bird instanceof Object); 


//zadanie 7
var myString = 'Text';
//zmienna jest instancja string instanceof String zwraca false
console.log(myString instanceof String); 

var myNumber = 10;
//zmienna jest instancja number wynik instanseof jest false
console.log(myString instanceof Number); 

//zadanie 8
//plik zadanie03

//zadanie 9
var Rectangle = function(lenght, width) {
    this.lenght = lenght;
    this.width = width;
}

Rectangle.prototype.getArea = function () {
    return this.lenght * this.width;
};
Rectangle.prototype.getPerimetr = function() {
    return 2*this.lenght + 2*this.width;
};

var rectangle1 = Object.create(Rectangle);
rectangle1.prototype.lenght = 3;
rectangle1.prototype.width = 2;

var rectangle2 = Object.create(Rectangle);
rectangle2.prototype.lenght = 2;
rectangle2.prototype.width = 4;

rectangle3 = Object.create(Rectangle);
rectangle3.prototype.lenght = 4;
rectangle3.prototype.width = 5;

//result false
//metoda hasOwnPropery sprawdzajac wlaściwości objetu nie uwzględnia włąściwości przejętych z prototypu
//a tylko te, które są bezpośrednio przepisane do objektu
console.log(rectangle1.hasOwnProperty('getArea'));
console.log(rectangle1.hasOwnProperty('getPerimetr'));

//dostep do metod poprzez uzycie prototype obj.prototype.method
console.log(rectangle2.prototype.getArea());
console.log(rectangle2.prototype.getPerimetr());

//zadanie 10
var Calculator = function () {
    this.history = [];
}

Calculator.prototype.add = function (num1, num2) {
    var result = num1 + num2;
    this.history.push('added ' + num1 + ' to ' + num2 + ' got ' + result);
    return result;
}

Calculator.prototype.multiply = function (num1, num2) {
    var result = num1 * num2;
    this.history.push('multiplied ' + num1 + ' with ' + num2 + ' got ' + result);
    return result;
}

Calculator.prototype.substract = function (num1, num2) {
    var result = num1 - num2;
    this.history.push('substracted ' + num1 + ' from ' + num2 + ' got ' + result);
    return result;
}

Calculator.prototype.divide = function (num1, num2) {
    var result = num1 / num2;
    this.history.push('divided ' + num1 + ' by ' + num2 + ' got ' + result);
    return result;
}

Calculator.prototype.printOperations = function () {
    this.history.forEach(i => {
        console.log(i);
    });
}

Calculator.prototype.clearOperations = function () {
    this.history.splice(0, this.history.length);
}

var calc = new Calculator();
calc.add(3, 5);
calc.multiply(3, 5);
calc.substract(3, 5);
calc.divide(6, 2);

calc.printOperations();

calc.clearOperations();
calc.add(2, 5);
console.log('test');
calc.printOperations();
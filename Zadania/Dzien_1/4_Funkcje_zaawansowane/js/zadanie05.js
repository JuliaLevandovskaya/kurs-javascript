//tworzenie funkcji
function sortArray() {

    //tworzenie tablicy
    var points = [41, 3, 6, 1, 114, 54, 64];

    //wywolanie metody sort na tablicy
    points.sort(function (a, b) {
        //sortowanie od najmniejszej liczby do najwiekszej
        return a - b;
    });

    //zwracanie posortowanej tablicy
    return points;
}

//wywolanie funkcji sortArray
sortArray();

//zadanie 2
function printMaxNumber() {
    var max = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        if (max < arguments[i]) {
            max = arguments[i];
        }
    }
    console.log(max);
}

printMaxNumber(1, 6, 7, 3, 89);

//zadanie 3
function countHello(number) {
    var counter = 1;
    var intervalId = setInterval(function () {
        if (counter === number) {
            clearInterval(intervalId);
        }
        console.log('Hello ' + counter);
        counter++;
    }, 1000);
}

countHello(2);

//funkcje mona wywowal przed i po definicji
sayHello();
function sayHello() {
    console.log('Czesc');
}
sayHello();

sayHello2(); //funkci nie mozna wywolac pzed definicja, zmienna jest undefined.
var sayHello2 = function () {
    console.log('Witaj');
}
sayHello2();
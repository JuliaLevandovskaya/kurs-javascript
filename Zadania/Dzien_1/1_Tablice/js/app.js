//dzien 1

//zadanie 1
var owoce = ['banan', 'gruszka', 'mango', 'anannas'];

console.log(owoce[0]);
console.log(owoce[owoce.length - 1]);

owoce.forEach((owoc, i) => {
    console.log(owoc);
});


//zadanie 2
function createArray(number) {
    var newArray = [];

    for (var counter = 1; counter <= number; counter++) {
        newArray.push(counter);
    }

    return newArray;
}

console.log("tablica z liczbami do 6 = " + createArray(6));
console.log("tablica z liczbami do 1 = " + createArray(1));
console.log("Test dla liczby ujemnej (powinna być pusta tablica) " + createArray(-6));
console.log("Test dla zera (powinna być pusta tablica) " + createArray(0));

//zadanie 3
function printTable(array) {
    for (var i in array) {
        console.log(array[i]);
    }
}

var table = [1, 2, 3, 4, 5];
printTable(table);

//zadanie 4
function multiply(array) {
    var counter = 1;
    array.forEach(function (element) {
        counter *= element;
    });
    return counter;
}

var array = [1, 2, 3, 4, 5, 6, 7];
console.log('counter ' + multiply(array));

//zadanie 5
function getEvenAvarage(array) {
    var sum = 0;
    var avg = null;
    var evenCounter = 0;

    array.forEach(function(el, i) {
        if (el % 2 == 0) {
            sum += el;
            evenCounter++;
        }
    });

    if (evenCounter > 0) {
        avg = sum / evenCounter;
    }

    return avg;
}

console.log(getEvenAvarage([1, 5, 3, 5, 5, 5, 7]));
console.log(getEvenAvarage([2, 8, 3, 7, 4]));

//zadanie 6
function sortArray(array) {
    return array.sort(function (a, b) { return a - b });
}

var array = sortArray([145, 11, 3, 64, 4, 6, 10]);
for (var i in array) {
    console.log(array[i]);
}

//zadanie 7 
function addArrays(array1, array2) {
    var resultArray = [];
    var additionalElements = [];

    while (array1.length !== array2.length) {
        if (array1.length > array2.length) {
            additionalElements.push(array1.pop());
        } else if (array2.length > array1.length) {
            additionalElements.push(array2.pop());
        }
    }
    for (var i in array1) {
        resultArray.push(array1[i] + array2[i]);
    }
    return resultArray.concat(additionalElements.reverse());
}

var array = addArrays([2, 3, 1, 5, 3, 5], [3, 1, 76, 1]);
var array2 = addArrays([4, 0, 1, 3, 4], [1, 9, 6, 7, 8, 17]);
var array3 = addArrays([8, 3, 22], [1, 3, 2]);
var array4 = addArrays([2, 3, 1, 5, 3, 5], [3, 1, 76, 1]);
console.log(array)
console.log(array2);
console.log(array3);
console.log(array4);



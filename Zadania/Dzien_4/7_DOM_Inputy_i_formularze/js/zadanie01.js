document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('#invoiceData').classList.add('hidden');
    
    var checkbox = document.querySelector('input[type=checkbox]');
    
    checkbox.addEventListener('click', function () { 
        document.querySelector('#invoiceData').classList.toggle('hidden');
    });
});
document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('.button').addEventListener('click', function () {
        
        var elementCounter = document.querySelector('ul').children.length + 1;

        var liElement = document.createElement('li');
        liElement.innerHTML = 'element nr ' + elementCounter;
        document.querySelector('ul').appendChild(liElement);
    });
    
});
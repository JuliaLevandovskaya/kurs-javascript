document.addEventListener('DOMContentLoaded', function () {
    var list1 = document.querySelector('#list1');
    var list2 = document.querySelector('#list2');
    var buttons = document.querySelectorAll('.moveBtn');
    for(var i = 0; i < buttons.length; i++){
        buttons[i].addEventListener('click', function () {
            var liElement = this.parentElement;
            if(liElement.parentElement.id === 'list1'){
                liElement.parentElement.removeChild(liElement);
                list2.appendChild(liElement);
            } else if(liElement.parentElement.id === 'list2'){
                liElement.parentElement.removeChild(liElement);
                list1.appendChild(liElement);
            }
            
        });
    }
    
});
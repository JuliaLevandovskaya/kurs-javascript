document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('#addBtn').addEventListener('click', function () {
        insertNewRow();
    });
});

function insertNewRow (){
    var table = document.querySelector('#orders');
    var row = table.insertRow(-1);
    var cellOrderNumber = row.insertCell(0);
    var cellitem = row.insertCell(1);
    var cellquantity = row.insertCell(2);

    cellOrderNumber.innerHTML = document.querySelector('#orderId').value;
    cellitem.innerHTML = document.querySelector('#item').value;
    cellquantity.innerHTML = document.querySelector('#quantity').value;
}